<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Message extends Model
{
    protected $guarded = [];
    protected $fillable = [
        'to',
        'from',
        'body'
    ];

    public function sender(){
        return $this->hasOne(User::class,'id','from');
    }

    public function receiver(){
        return $this->hasOne(User::class,'to','id');
    }

    public static function getConversation($id){
        self::where('to',Auth::id())->where('from',$id)->update(['is_read' => true]);

        $messages = self::where(function($query) use($id){
            $query->where('to',$id)->where('from',Auth::id());
        })->orWhere(function($query) use($id){
            $query->where('to',Auth::id())->where('from',$id);
        })->get();

        return $messages;
    }
}
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/contacts', 'ContactsController@get')->name('contacts');
Route::get('/conversation/{id}', 'ContactsController@getMessagesFor')->name('get-messages');
Route::post('/send-message', 'ContactsController@sendMessage')->name('send-messages');
